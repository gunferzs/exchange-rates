package ru.gunferzs.exchangerates.presentations.presenters

import android.support.v7.widget.RecyclerView
import ru.gunferzs.exchangerates.presentations.presenters.base.IPresenter
import ru.gunferzs.exchangerates.presentations.ui.base.IProgressView
import ru.gunferzs.exchangerates.presentations.ui.base.IView

interface IMainPresenter : IPresenter {

    fun clickMenuUpdate()
    fun update()

    interface IMainView : IView, IProgressView {
        fun showError(error: String?, show: Boolean)
        fun setAdapter(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>)
        fun savePositionList()
        fun restorePostionList()
    }
}