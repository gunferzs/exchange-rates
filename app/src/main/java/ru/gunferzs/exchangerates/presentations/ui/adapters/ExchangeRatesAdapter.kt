package ru.gunferzs.exchangerates.presentations.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.gunferzs.exchangerates.R
import ru.gunferzs.exchangerates.web.models.Stock

class ExchangeRatesAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listItems: List<Stock>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_exchange_rates_item, parent, false)
        return ExchangeRatesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    fun Double.format(digits: Int) = java.lang.String.format("%.${digits}f", this)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var item = listItems[position]
        var itemHolder = holder as ExchangeRatesViewHolder
        holder.nameCurrency.text = item.name
        holder.priceCurrency.text = item.volume.toString()
        holder.amountCurrency.text = item.price.amount.format(2)
    }

    class ExchangeRatesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var nameCurrency: TextView
        var priceCurrency: TextView
        var amountCurrency: TextView

        init {
            nameCurrency = view.findViewById(R.id.name_currency)
            priceCurrency = view.findViewById(R.id.price_currency)
            amountCurrency = view.findViewById(R.id.amount_currency)
        }
    }
}