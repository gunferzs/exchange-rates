package ru.gunferzs.exchangerates.presentations.ui.base

interface IProgressView {
    fun showProgress()
    fun hideProgress()
}