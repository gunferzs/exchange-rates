package ru.gunferzs.exchangerates.presentations.presenters.impl

import android.os.Bundle
import android.os.Handler
import ru.gunferzs.exchangerates.domain.executables.IExchangeRatesExecutable
import ru.gunferzs.exchangerates.domain.executables.impl.ExchangeRatesExecutable
import ru.gunferzs.exchangerates.domain.executors.impl.MainThread
import ru.gunferzs.exchangerates.domain.executors.impl.ThreadExecutor
import ru.gunferzs.exchangerates.presentations.presenters.IMainPresenter
import ru.gunferzs.exchangerates.presentations.presenters.base.BasePresenter
import ru.gunferzs.exchangerates.presentations.ui.adapters.ExchangeRatesAdapter
import ru.gunferzs.exchangerates.web.models.ExchangeRatesModel
import java.util.*


class MainPresenter(view: IMainPresenter.IMainView) : BasePresenter<IMainPresenter.IMainView>(view), IMainPresenter, IExchangeRatesExecutable.IExchangeResultCallback {

    companion object {
        const val TIME_DELAY = 15000L
        const val TIME_PERIOD = 15000L
    }

    protected var isRunning: Boolean = false
    val mainThread = MainThread()
    var executable = ExchangeRatesExecutable(this, ThreadExecutor.instance, mainThread)
    lateinit var timer: Timer

    override fun onCreate(savedInstanceState: Bundle?) {

    }

    override fun onStart() {

    }

    override fun onResume() {
        var handler = Handler()

        handler.postDelayed({
            view.showError("Errror: my error", true)
        }, 10000L)

        view.showProgress()
        executable.execute()
        timer = Timer()

        timer.schedule(object : TimerTask() {

            override fun run() {
                update()
            }
        }, TIME_DELAY, TIME_PERIOD)
    }

    @Synchronized
    override fun update() {
        isRunning = true
        view.savePositionList()
        executable.execute()
    }

    override fun onStop() {
        timer?.cancel()
    }

    override fun onDestroy() {

    }

    override fun clickMenuUpdate() {
        if (!isRunning) {
            view.showProgress()
            update()
        }
    }

    override fun onSuccess(model: ExchangeRatesModel?) {
        view.showError("", false)

        model?.let {
            val adapter = ExchangeRatesAdapter()
            adapter.listItems = it.stock
            view.setAdapter(adapter)
            view.restorePostionList()
        }

    }

    override fun onError(error: String) {
        view.showError(error, true)
    }

    override fun onComplete() {
        view.hideProgress()
        isRunning = false
    }

}