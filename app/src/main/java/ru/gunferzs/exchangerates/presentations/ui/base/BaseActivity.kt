package ru.gunferzs.exchangerates.presentations.ui.base

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import ru.gunferzs.exchangerates.R
import ru.gunferzs.exchangerates.presentations.presenters.base.IPresenter

abstract class BaseActivity<View : IView, Presenter : IPresenter> : AppCompatActivity() {

    protected lateinit var presenter: Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        layoutInflater.inflate(getLayout(), container)
        presenter = createPresenter()
        afterOnCreate(savedInstanceState)
    }

    @LayoutRes
    abstract fun getLayout(): Int

    abstract fun createPresenter(): Presenter
    abstract fun getView(): View
    abstract fun afterOnCreate(savedInstanceState: Bundle?)
}