package ru.gunferzs.exchangerates.presentations.ui.activities

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.view_table_exchange_rates.*
import ru.gunferzs.exchangerates.R
import ru.gunferzs.exchangerates.presentations.presenters.IMainPresenter
import ru.gunferzs.exchangerates.presentations.presenters.impl.MainPresenter
import ru.gunferzs.exchangerates.presentations.ui.adapters.HorizontalDividerDecorator
import ru.gunferzs.exchangerates.presentations.ui.base.BaseActivity

class MainActivity : BaseActivity<IMainPresenter.IMainView, IMainPresenter>(), IMainPresenter.IMainView {

    var position = 0
    var topView = 0

    lateinit var linearLayoutManager: LinearLayoutManager

    @LayoutRes
    override fun getLayout(): Int {
        return R.layout.view_table_exchange_rates
    }

    override fun createPresenter(): IMainPresenter {
        return MainPresenter(this)
    }

    override fun getView(): IMainPresenter.IMainView {
        return this
    }

    override fun afterOnCreate(savedInstanceState: Bundle?) {
        linearLayoutManager = LinearLayoutManager(this)
        exchange_rates_list.layoutManager = linearLayoutManager
        exchange_rates_list.addItemDecoration(HorizontalDividerDecorator(this))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        presenter.clickMenuUpdate()
        return super.onOptionsItemSelected(item)
    }

    override fun showError(error: String?, show: Boolean) {
        error_container.visibility = if (show) View.VISIBLE else View.GONE
        error_text.text = error
    }

    override fun setAdapter(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
        exchange_rates_list.adapter = adapter
    }

    override fun showProgress() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_bar.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun savePositionList() {
        position = linearLayoutManager.findFirstVisibleItemPosition()
        var startView = exchange_rates_list.getChildAt(0)

        topView = if (startView == null) 0 else (startView.top - exchange_rates_list.paddingTop)
    }

    override fun restorePostionList() {
        if (position != -1) {
            linearLayoutManager.scrollToPositionWithOffset(position, topView)
        }
    }
}