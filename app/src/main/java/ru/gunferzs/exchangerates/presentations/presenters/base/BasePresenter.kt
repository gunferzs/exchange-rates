package ru.gunferzs.exchangerates.presentations.presenters.base

import ru.gunferzs.exchangerates.presentations.ui.base.IView

abstract class BasePresenter<View : IView>(protected var view: View) : IPresenter