package ru.gunferzs.exchangerates.presentations.presenters.base

import android.os.Bundle

interface IPresenter {
    fun onCreate(savedInstanceState: Bundle?)
    fun onStart()
    fun onResume()
    fun onStop()
    fun onDestroy()
}