package ru.gunferzs.exchangerates.web.web

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.gunferzs.exchangerates.web.models.ExchangeRatesModel

class ServerApi {

    var webService: IWebService

    companion object {
        const val url: String = "http://phisix-api3.appspot.com/"

        val instance: ServerApi by lazy {
            Holder.INSTANCE
        }
    }

    private object Holder {
        val INSTANCE = ServerApi()
    }

    init {
        var retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        webService = retrofit.create(IWebService::class.java)
    }

    fun getExchangeRates(): Response<ExchangeRatesModel> {
        var call = webService.getExchangeRates()

        return call.execute()
    }

}