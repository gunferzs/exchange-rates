package ru.gunferzs.exchangerates.web.models

data class Price(var currency: String, var amount: Double)