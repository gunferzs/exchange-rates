package ru.gunferzs.exchangerates.web.models

interface IResultCallback<Model, Error> {
    fun onSuccess(model: Model?)
    fun onError(error: Error)
    fun onComplete()
}