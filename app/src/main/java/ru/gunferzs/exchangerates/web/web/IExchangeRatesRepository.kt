package ru.gunferzs.exchangerates.web.web

import retrofit2.Response
import ru.gunferzs.exchangerates.web.models.ExchangeRatesModel

interface IExchangeRatesRepository : IRepository {
    fun getExchangeRates(): Response<ExchangeRatesModel>
}