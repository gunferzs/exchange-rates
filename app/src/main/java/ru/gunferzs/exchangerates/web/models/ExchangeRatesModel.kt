package ru.gunferzs.exchangerates.web.models

import com.google.gson.annotations.SerializedName

data class ExchangeRatesModel(var stock: List<Stock>, @SerializedName("as_of") var asOf: String)