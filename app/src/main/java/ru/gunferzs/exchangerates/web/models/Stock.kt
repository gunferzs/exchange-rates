package ru.gunferzs.exchangerates.web.models

import com.google.gson.annotations.SerializedName

data class Stock(var name: String, var price: Price, @SerializedName("percent_change") var percentChange: Double, var volume: Int, var symbol: String)