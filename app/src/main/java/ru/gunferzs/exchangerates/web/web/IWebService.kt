package ru.gunferzs.exchangerates.web.web

import retrofit2.Call
import retrofit2.http.GET
import ru.gunferzs.exchangerates.web.models.ExchangeRatesModel

interface IWebService {

    @GET("stocks.json")
    fun getExchangeRates(): Call<ExchangeRatesModel>
}