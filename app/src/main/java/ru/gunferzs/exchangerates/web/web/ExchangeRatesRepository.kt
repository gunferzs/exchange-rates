package ru.gunferzs.exchangerates.web.web

import retrofit2.Response
import ru.gunferzs.exchangerates.web.models.ExchangeRatesModel

class ExchangeRatesRepository : IExchangeRatesRepository {

    override fun getExchangeRates(): Response<ExchangeRatesModel> {
        return ServerApi.instance.getExchangeRates()
    }
}