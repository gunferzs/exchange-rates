package ru.gunferzs.exchangerates.domain.executors.base

interface IMainThread {
    fun post(runnable: Runnable)
}