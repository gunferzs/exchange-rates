package ru.gunferzs.exchangerates.domain.executables.base

interface IExecutable {
    fun execute()
}