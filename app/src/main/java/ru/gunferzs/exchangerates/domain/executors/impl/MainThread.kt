package ru.gunferzs.exchangerates.domain.executors.impl

import android.os.Handler
import android.os.Looper
import ru.gunferzs.exchangerates.domain.executors.base.IMainThread


class MainThread() : IMainThread {

    private var mHandler: Handler

    init {
        mHandler = Handler(Looper.getMainLooper())
    }

    private object Holder {
        val INSTANCE = MainThread()
    }

    companion object {
        val instance: MainThread by lazy {
            Holder.INSTANCE
        }
    }

    override fun post(runnable: Runnable) {
        mHandler.post(runnable)
    }
}