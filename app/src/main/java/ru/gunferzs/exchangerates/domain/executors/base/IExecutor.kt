package ru.gunferzs.exchangerates.domain.executors.base

import ru.gunferzs.exchangerates.domain.executables.base.BaseExecutable

interface IExecutor {
    fun execute(executable: BaseExecutable)
}