package ru.gunferzs.exchangerates.domain.executables.impl

import retrofit2.Response
import ru.gunferzs.exchangerates.domain.executables.IExchangeRatesExecutable
import ru.gunferzs.exchangerates.domain.executables.base.BaseExecutable
import ru.gunferzs.exchangerates.domain.executors.base.IExecutor
import ru.gunferzs.exchangerates.domain.executors.base.IMainThread
import ru.gunferzs.exchangerates.web.models.ExchangeRatesModel
import ru.gunferzs.exchangerates.web.web.ExchangeRatesRepository
import ru.gunferzs.exchangerates.web.web.IExchangeRatesRepository

class ExchangeRatesExecutable(var callbackPresenter: IExchangeRatesExecutable.IExchangeResultCallback, threadExecutor: IExecutor, mainThread: IMainThread) : BaseExecutable(threadExecutor, mainThread) {

    var repository: IExchangeRatesRepository

    init {
        repository = ExchangeRatesRepository()
    }

    private fun onSuccess(response: Response<ExchangeRatesModel>) {
        mainThread.post(Runnable { callbackPresenter.onSuccess(response.body()) })
    }

    private fun onComplete() {
        mainThread.post(Runnable { callbackPresenter.onComplete() })
    }

    private fun onError(response: Response<ExchangeRatesModel>) {
        var error: String = "Error (" + response.code() + ") "

        response.errorBody()?.let {
            error += it.string()
        }

        mainThread.post(Runnable { callbackPresenter.onError(error) })
    }

    override fun run() {
        var response = repository.getExchangeRates()

        if (response.isSuccessful) {
            onSuccess(response)
            onComplete()

            return
        }

        onError(response)
        onComplete()
    }

}