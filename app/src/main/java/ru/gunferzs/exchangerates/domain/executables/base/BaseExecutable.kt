package ru.gunferzs.exchangerates.domain.executables.base

import ru.gunferzs.exchangerates.domain.executors.base.IExecutor
import ru.gunferzs.exchangerates.domain.executors.base.IMainThread


abstract open class BaseExecutable(open var threadExecutor: IExecutor, open var mainThread: IMainThread) : IExecutable {

    @Volatile
    protected var isCanceled: Boolean = false

    @Volatile
    protected var isRunning: Boolean = false

    abstract fun run()

    fun cancel() {
        isCanceled = true
        isRunning = false
    }

    fun onFinished() {
        isRunning = false
        isCanceled = false
    }

    override fun execute() {
        isRunning = true
        threadExecutor.execute(this)
    }
}