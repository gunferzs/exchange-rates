package ru.gunferzs.exchangerates.domain.executables

import ru.gunferzs.exchangerates.domain.executors.base.IExecutor
import ru.gunferzs.exchangerates.web.models.ExchangeRatesModel
import ru.gunferzs.exchangerates.web.models.IResultCallback

interface IExchangeRatesExecutable : IExecutor {

    interface IExchangeResultCallback : IResultCallback<ExchangeRatesModel, String> {
    }
}